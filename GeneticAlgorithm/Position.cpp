#include "Position.h"

Position::Position(int x, int y): x(x), y(y)
{
}

Position * Position::getLookPosition(byte angle)
{
	switch (angle)
	{
	case 0:
		return new Position(x,                     (y == 0) ? 7 : (y - 1));  // Top
	case 1:
		return new Position((x > 6) ? 0 : (x + 1), (y == 0) ? 7 : (y - 1));  // Top-Right
	case 2:
		return new Position((x > 6) ? 0 : (x + 1), y);                       // Right
	case 3:
		return new Position((x > 6) ? 0 : (x + 1), (y > 6) ? 0 : (y + 1));   // Bottom-Right
	case 4:
		return new Position(x,                     (y > 6) ? 0 : (y + 1));   // Bottom
	case 5:
		return new Position((x == 0) ? 7 : (x - 1), (y > 6) ? 0 : (y + 1));  // Bottom-Left
	case 6:
		return new Position((x == 0) ? 7 : (x - 1), y);                      // Left
	case 7:
		return new Position((x == 0) ? 7 : (x - 1), (y == 0) ? 7 : (y - 1)); // Top-Left
	default:
		return nullptr;
	}
}

bool Position::operator==(const Position * right) {
	return x == right->x && y == right->y;
}

bool Position::operator!=(const Position * right) {
	return !(this == right);
}
