#include "WorldMap.h"
#include <Adafruit_NeoPixel.h>

Adafruit_NeoPixel strip = Adafruit_NeoPixel(64, A0, NEO_GRB + NEO_KHZ800);
WorldMap* worldMap;

int xSize = 8;
int ySize = 8;

int xScreenSize = 8;
int yScreenSize = 8;

// the setup function runs once when you press reset or power the board
void setup() {
	worldMap = new WorldMap(xSize, ySize, freemem);

	strip.begin();
	strip.setBrightness(15);
	strip.show(); // Initialize all pixels to 'off'
}

// the loop function runs over and over again until power down or reset
void loop() {
	delay(20);

	worldMap->run();

	for (int x = 0; x < xScreenSize; x++) {
		for (int y = 0; y < yScreenSize; y++) {
			byte* color = worldMap->getCell(x, y)->getColor();
			strip.setPixelColor(x + y * yScreenSize, color[0], color[1], color[2]);
			delete color;
		}
	}

	strip.show();
}

extern int  __bss_end;
extern int  *__brkval;
int freemem()
{
	int free_memory;
	if ((int)__brkval == 0)
		free_memory = ((int)&free_memory) - ((int)&__bss_end);
	else
		free_memory = ((int)&free_memory) - ((int)__brkval);
	return free_memory;
}
