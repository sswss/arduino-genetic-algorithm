#include "WorldMap.h"


WorldMap::WorldMap(int xSize, int ySize, int freemem()): xSize(xSize), ySize(ySize), getMem(freemem), size(xSize * ySize) {
	cellEmptyCell = new EmptyCell;
	cellFood = new Food;
	cellPoison = new Poison;
	cellWall = new Wall;

	this->map = new byte[size]();
	for (int x = 0; x < xSize; x++) {
		for (int y = 0; y < ySize; y++) {
			this->setCell(x, y, *cellEmptyCell);
		}
	}

	this->generateObjects(CellType::WALL, size * 0.1);
	this->generateObjects(CellType::FOOD, size * 0.2);
	this->generateObjects(CellType::POISON, size * 0.1);

	this->creeps = new Creep*[5];
	for (byte i = 0; i < 5; i++) {
		Creep* creep = new Creep(i, getFreeCell(), *this);
		this->setCell(*creep);
		this->creeps[i] = creep;
	}
}

WorldMap::~WorldMap() {}

Cell * WorldMap::getCell(int x, int y) {
	int position = getIntPosition(x, y); 
	int type = map[position];
	Cell * cell;

	if (type == 1) {
		cell = cellEmptyCell;
	} else if (type == 2) {
		cell = cellFood;
	} else if (type == 3) {
		cell = cellWall;
	} else if (type == 4) {
		cell = cellPoison;
	} else if (type <= 10) {
		cell = creeps[type - 5];
	} else {
		cell = nullptr;
	}

	return cell;
}

Cell * WorldMap::getCell(Position * position) {
	return getCell(position->x, position->y);
}

bool WorldMap::setCell(int x, int y, StaticCell & cell) {
	int position = getIntPosition(x, y);
	this->map[position] = static_cast<int>(cell.getType());

	return true;
}

bool WorldMap::setCell(Position * position, StaticCell & cell) {
	return setCell(position->x, position->y, cell);
}

bool WorldMap::setCell(Creep & creep) {
	int position = getIntPosition(*creep.position);
	this->map[position] = static_cast<int>(creep.getType()) + creep.id;

	return true;
}

void WorldMap::run() {
	for (byte i = 0; i < 5; i++) {
		Creep * creep = creeps[i];
		creep->addEnergy(10);
		creep->run();

		if (creep->getHealth() <= 0) {
			Creep * parent1 = getBestCreep();
			Creep * parent2 = getBestCreep(parent1);

			Creep * newCreep = new Creep(i, getFreeCell(), *this, parent1, parent2);
			setCell(*newCreep);
			this->setCell(creep->position, *cellEmptyCell);
			
			delete this->creeps[i];
			this->creeps[i] = newCreep;
		}
	}
}

void WorldMap::printMem(String message) {
	// Need to add breakpoint with action "{message}: {getMem()}" to print memory report
	// Note that message also use 1 byte of memory per each character

	message = message;
}

void WorldMap::generateObjects(CellType type, byte amount) {
	for (byte i = 0; i < amount; i++) {
		Position * position = getFreeCell();

		switch (type) {
			case CellType::FOOD:
				this->setCell(position, *cellFood);
				break;
			case CellType::WALL:
				this->setCell(position, *cellWall);
				break;
			case CellType::POISON:
				this->setCell(position, *cellPoison);
				break;
			default:
				break;
		}

		delete position;
	}
}

int WorldMap::getIntPosition(int x, int y) {
	Position * position = getPosition(x, y);
	int res = position->x + position->y * xSize;
	delete position;

	return res;
}

int WorldMap::getIntPosition(Position & position) {
	return getIntPosition(position.x, position.y);
}

Position * WorldMap::getPosition(int x, int y) {
	while (x >= xSize) {
		x -= xSize;
	};

	while (x < 0) {
		x += xSize;
	};

	while (y >= ySize) {
		y -= ySize;
	};

	while (y < 0) {
		y += ySize;
	};

	return new Position(x, y);
}

Position * WorldMap::getPosition(Position & position) {
	return getPosition(position.x, position.y);
}

Creep * WorldMap::getBestCreep(Creep * exclude) {
	Creep * best;
	
	for (byte i = 0; i < 5; i++) {
		if (best == nullptr || best->getGeneration() < creeps[i]->getGeneration()) {
			if (exclude == nullptr || exclude->position != creeps[i]->position) {
				best = creeps[i];
			}
		}
	}

	return best;
}

Position * WorldMap::getFreeCell() {
	Position * position = new Position(0, 0);

	do {
		position->x = random(xSize);
		position->y = random(ySize);
	} while (getCell(position)->getType() != CellType::EMPTY);

	return position;
}
