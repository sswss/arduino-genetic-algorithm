#include "EmptyCell.h"

CellType EmptyCell::getType() {
	return CellType::EMPTY;
}

byte* EmptyCell::getColor()
{
	return new byte[3]{ 0, 0, 0 };
}

bool EmptyCell::isCanMove()
{
	return true;
}
