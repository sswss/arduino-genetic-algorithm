#include "Creep.h"

Creep::Creep(byte id, Position * position, WorldMap & map) : id(id), position(position), map(map) {
	this->actions = new byte[CREEP_ACTIONS]();

	for (byte i = 0; i < CREEP_ACTIONS; i++) {
		this->actions[i] = random(CREEP_ACTIONS / 2);
	}

	this->health = 100;
	this->energy = 0;
	this->age = 0;
	this->generation = 1;
}

Creep::Creep(byte id, Position * position, WorldMap & map, Creep * parent1, Creep * parent2) : id(id), position(position), map(map) {
	this->actions = new byte[CREEP_ACTIONS]();

	// Get actions from parent
	for (byte i = 0; i < CREEP_ACTIONS; i++) {
		this->actions[i] = (i % 2 == 0) ? parent1->actions[i] : parent2->actions[i];
	}

	// Mutate random action
	this->actions[random(CREEP_ACTIONS)] = random(CREEP_ACTIONS / 2);

	// Increase generation number
	this->generation = parent1->age + parent2->age;

	this->health = 100;
	this->energy = 0;
	this->age = 0;
}

Creep::~Creep() {
	delete actions;
	delete position;
}

void Creep::run() {
	addHealth(-1);
	age++;

	byte nb = 10;
	while (--nb > 0 && health > 0) {
		if (!this->executeAction()) {
			return;
		}
	}
}

int Creep::getAge() {
	return age;
}

int Creep::getGeneration() {
	return generation + age + health;
}

byte* Creep::getColor() {
	float b = ((float)min(200, health) / 200) * 250;

	return new byte[3]{ 0, 0, (byte)max(b, 10) };
}

CellType Creep::getType() {
	return CellType::CREEP;
}

void Creep::addHealth(int health) {
	if ((int)this->health + health > 200) {
		this->health = 200;
	} else if ((int)this->health + health < 0) {
		this->health = 0;
	} else {
		this->health += health;
	}
}

byte Creep::getHealth() {
	return health;
}

void Creep::addEnergy(byte energy) {
	this->energy += energy;
}

bool Creep::doSetAngle(byte angle) {
	if (energy < 2) {
		return false;
	}
	energy -= 2;

	while (angle < 0) {
		angle += 8;
	}
	while (angle > 7) {
		angle -= 8;
	}

	this->angle = angle;
	this->setAction(action + 1);

	return true;
}

bool Creep::doSetAction(byte action) {
	if (energy < 1) {
		return false;
	}
	energy -= 1;

	setAction(action);

	return true;
}

bool Creep::doEat() {
	if (energy < 4) {
		return false;
	}
	energy -= 4;

	Position * look = position->getLookPosition(angle);
	if (look == nullptr) {
		setAction(action + 6);
	} else {
		Cell* cell = map.getCell(look);
		this->setAction(action + static_cast<int>(cell->getType()));

		switch (cell->getType()) {
			case CellType::FOOD:
				addHealth(10);
				map.setCell(look, *map.cellEmptyCell);
				map.generateObjects(random(100) > 50 ? CellType::FOOD : CellType::POISON, 1);
				break;
			case CellType::POISON:
				map.setCell(look, *map.cellFood);
			default:
				break;
		}

		delete look;
	}

	return true;
}

void Creep::setAction(byte action) {
	while (action < 0) {
		action += 36;
	}
	while (action > 35) {
		action -= 36;
	}

	this->action = action;
}

bool Creep::executeAction() {
	byte action = this->actions[this->action];
	if (action == 0) {
		// move forward [6] (-10 health if poison)
		return this->doMove();
	} else if (action == 1) {
		// turl left [2]
		return this->doSetAngle(angle - 1);
	} else if (action == 2) {
		// turn right [2]
		return this->doSetAngle(angle + 1);
	} else if (action == 3) {
		// look forward [2]
		return this->doLook();
	} else if (action == 4) {
		// use [4] (eat food or turn poison into food)
		return this->doEat();
	} else {
		// move current cell to specified amount of cells [1]
		return this->doSetAction(action + action - 4);
	}
}

bool Creep::doMove() {
	if (energy < 6) {
		return false;
	}
	energy -= 6;

	Position * look = position->getLookPosition(angle);
	if (look == nullptr) {
		setAction(action + 6);
	} else {
		Cell * cell = map.getCell(look);
		this->setAction(action + static_cast<int>(cell->getType()));

		if (cell->isCanMove()) {
			if (cell->getType() == CellType::POISON) {
				addHealth(-10);
				map.generateObjects(CellType::POISON, 1);
			}

			if (map.setCell(*this)) {
				map.setCell(position, *map.cellEmptyCell);

				position->x = look->x;
				position->y = look->y;
			}
		}

		delete look;
	}

	return true;
}

bool Creep::doLook() {
	if (energy < 2) {
		return false;
	}
	energy -= 2;

	Position * look = position->getLookPosition(angle);
	if (look == nullptr) {
		setAction(action + 6);
	} else {
		Cell * cell = map.getCell(look);
		setAction(action + static_cast<int>(cell->getType()));

		delete look;
	}

	return true;
}
