#ifndef FoodDef
#define FoodDef

#include "StaticCell.h"

class Food :
	public StaticCell
{
public:
	Food() {};
	~Food() {};

	byte* getColor();
	CellType getType();
};

#endif // !FoodDef