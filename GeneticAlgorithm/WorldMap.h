#ifndef MapDef
#define MapDef

#include "Food.h"
#include "Wall.h"
#include "Poison.h"
#include "Creep.h"
#include "EmptyCell.h"
#include "Cell.h"
#include "CellType.h"
#include "Position.h"
#include "StaticCell.h"

class Creep;

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif

class WorldMap {
public:
	WorldMap(int xSize, int ySize, int freemem());
	~WorldMap();

	const int xSize;
	const int ySize;
	const int size;

	EmptyCell * cellEmptyCell;
	Food * cellFood;
	Wall * cellWall;
	Poison * cellPoison;

	Cell * getCell(int x, int y);
	Cell * getCell(Position * position);

	bool setCell(int x, int y, StaticCell & cell);
	bool setCell(Position * position, StaticCell & cell);

	bool setCell(Creep & creep);

	void run();
	void printMem(String message);
	void generateObjects(CellType type, byte amount);

	int getIntPosition(int x, int y);
	int getIntPosition(Position & position);
	Position * getPosition(int x, int y);
	Position * getPosition(Position & position);

	Creep * getBestCreep(Creep * exclude = nullptr);

	Position * getFreeCell();
private:
	byte * map;

	Creep** creeps;
	int(*getMem)();
};

#endif // !MapDef