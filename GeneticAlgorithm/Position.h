#ifndef PositionDef
#define PositionDef

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif

class Position
{
public:
	int x;
	int y;

	Position(int x, int y);

	Position * getLookPosition(byte angle);

	bool operator==(const Position * right);
	bool operator!=(const Position * right);
};

#endif // !PositionDef