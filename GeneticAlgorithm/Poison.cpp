#include "Poison.h"

byte* Poison::getColor()
{
	return new byte[3]{ 30, 0, 0 };
}

CellType Poison::getType() {
	return CellType::POISON;
}

bool Poison::isCanMove() {
	return true;
}
