#ifndef CELL_H
#define CELL_H

#include "CellType.h"
class WorldMap;

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif

class Cell
{
public:
	Cell() {};
	virtual ~Cell();
	virtual CellType getType() = 0;
	virtual byte* getColor() = 0;
	virtual bool isCanMove();
};

#endif // !CELL_H