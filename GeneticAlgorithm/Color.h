#ifndef ColorDef
#define ColorDef

struct Color
{
	byte red = 0;
	byte green = 0;
	byte blue = 0;
};

#endif // !ColorDef
