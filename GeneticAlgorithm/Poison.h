#ifndef PoisonDeff
#define PoisonDeff

#include "StaticCell.h"

class Poison :
	public StaticCell
{
public:
	Poison() {};
	~Poison() {};

	byte* getColor();
	CellType getType();
	bool isCanMove();
};

#endif // !PoisonDeff