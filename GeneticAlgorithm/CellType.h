#ifndef CELLTYPE_H
#define CELLTYPE_H

enum class CellType : int {
	EMPTY = 1,
	FOOD = 2,
	WALL = 3,
	POISON = 4,
	CREEP = 5,
};

#endif // !CELLTYPE_H