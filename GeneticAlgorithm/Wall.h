#ifndef WallDef
#define WallDef

#include "StaticCell.h"

class Wall :
	public StaticCell
{
public:
	Wall() {};
	~Wall() {};

	byte* getColor();
	CellType getType();
};

#endif // !WallDef