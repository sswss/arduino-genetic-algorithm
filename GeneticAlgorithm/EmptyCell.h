#ifndef EMPTYCELL_H
#define EMPTYCELL_H

#include "StaticCell.h"

class EmptyCell :
	public StaticCell
{
public:
	EmptyCell() {};
	~EmptyCell() {};

	CellType getType();
	byte* getColor();
	bool isCanMove();
};
#endif // !EMPTYCELL_H

