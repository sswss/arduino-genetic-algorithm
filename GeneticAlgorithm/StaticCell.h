#ifndef StaticCellDef
#define StaticCellDef

#include "Cell.h"

class StaticCell : public Cell {
public:
	virtual ~StaticCell() {};
};

#endif // !StaticCellDef

