#ifndef CREEP_H
#define CREEP_H

#include "WorldMap.h"
#include "Cell.h"
#include "Position.h"

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif

class Creep :
	public Cell
{
private:
	byte action = 0; // Current action
	byte angle = 0; // 0-7
	int age;
	int generation;
	byte health;

	bool executeAction();
protected:
	bool doMove();
	bool doLook();
	bool doSetAngle(byte angle);
	bool doSetAction(byte action);
	bool doEat();

	WorldMap& map;
public:
	static const byte CREEP_ACTIONS = 32;
	const byte id;

	byte * actions; // List of all creep actions
	Position* position;
	byte energy;

	Creep(byte id, Position* position, WorldMap& map);
	Creep(byte id, Position* position, WorldMap& map, Creep * parent1, Creep * parent2);
	~Creep();

	void run();

	int getAge();
	int getGeneration();
	byte* getColor();
	CellType getType();

	void addHealth(int health);
	byte getHealth();

	void addEnergy(byte energy);
	void setAction(byte action);
};


#endif // !CREEP_H